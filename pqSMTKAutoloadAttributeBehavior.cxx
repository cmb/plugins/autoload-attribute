//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKAutoloadAttributeBehavior.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKRenderResourceBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"

// Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqObjectBuilder.h"
#include "pqServerManagerModel.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxyDefinitionManager.h"
#include "vtkSMSessionProxyManager.h"

#include <vtksys/SystemTools.hxx>

namespace
{
// We can access environment variables using vtksys's cross-platform environment
// query.
std::string dataRootFromEnvironment()
{
  return std::string(vtksys::SystemTools::GetEnv("MY_DATA_DIRECTORY"));
}

// The precompiler definition for DATA_DIR is passed in during compilation (see
// the associated CMakeLists.txt for the assignment of this variable).
std::string dataRoot = std::string(DATA_DIR);
}

// A pointer to our singleton instance.
static pqSMTKAutoloadAttributeBehavior* g_instance = nullptr;

pqSMTKAutoloadAttributeBehavior::pqSMTKAutoloadAttributeBehavior(QObject* parent)
  : Superclass(parent)
{
  // Upon creation of the class's sole instance, connect pqSMTKBehavior's
  // "addedManagerOnServer" to our "loadAttribute" method. Each time a server is
  // instantiated, our method will be called on the newly created server.
  QObject::connect(pqSMTKBehavior::instance(),
    (void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)) & pqSMTKBehavior::addedManagerOnServer,
    this, &pqSMTKAutoloadAttributeBehavior::loadAttribute);

  // In the event that there are already servers present when this class is
  // created, visit each extant server and call "loadAttribute" on it.
  pqSMTKBehavior::instance()->visitResourceManagersOnServers(
    [this](pqSMTKWrapper* wrapper, pqServer* server)
    {
      this->loadAttribute(wrapper, server);

      // The return value of this functor determines whether or not visitation
      // should terminate. We always return false because there is no condition
      // under which we should terminate our visitation early.
      return false;
    });
}

pqSMTKAutoloadAttributeBehavior* pqSMTKAutoloadAttributeBehavior::instance(QObject* parent)
{
  // If the singleton instance has not been created, create it now.
  if (!g_instance)
  {
    g_instance = new pqSMTKAutoloadAttributeBehavior(parent);
  }

  // If the singleton does not have a parent and the accessor's input parameter
  // is set, reparent the instance with the input parent.
  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  // Return the sole instance of this class.
  return g_instance;
}

pqSMTKAutoloadAttributeBehavior::~pqSMTKAutoloadAttributeBehavior()
{
  // Unset the pointer to our single instance if it is not already unset.
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  // Remove all connections from this instance.
  QObject::disconnect(this);
}

void pqSMTKAutoloadAttributeBehavior::loadAttribute(pqSMTKWrapper*, pqServer* server)
{
  // Access a file from the data directory.
  std::string myDataRoot = dataRootFromEnvironment();
  if (myDataRoot.empty())
  {
    std::cout << "Environment variable \"MY_DATA_DIRECTORY\" is unset.\n"
              << "Using default data directory.\n";
    myDataRoot = dataRoot;
  }
  std::string fileName = myDataRoot + "/HydraTemplateV1.sbt";

  // If the application core is valid...
  auto pqCore = pqApplicationCore::instance();
  if (pqCore != nullptr)
  {
    // ...access its object builder.
    auto builder = pqCore->getObjectBuilder();

    // Construct an attribute reader using the object builder.
    pqSMTKResource* src = dynamic_cast<pqSMTKResource*>(
      builder->createSource("sources", "SMTKAttributeReader", server));

    // Set the attribute reader's file name.
    vtkSMPropertyHelper(src->getProxy(), "FileName").Set(fileName.c_str());

    // Render the newly created source.
    //
    // NOTE: pqSMTKRenderResourceBehavior doesn't need to be constructed during
    //       startup because it is functionally stateless (it holds an intenral
    //       class instance that is used in a stateless way). It could be added
    //       to the list of required behaviors accessed at startup(), though.
    pqSMTKRenderResourceBehavior::instance()->renderPipelineSource(src);
  }
}
