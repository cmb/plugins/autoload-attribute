//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKAutoloadAttributeBehavior_h
#define pqSMTKAutoloadAttributeBehavior_h

#include "smtk/extension/paraview/appcomponents/Exports.h"
#include <QObject>

class pqServer;
class pqSMTKWrapper;

/** \brief Autoload an attribute whenever a new server is created
  */
class SMTKPQCOMPONENTSEXT_EXPORT pqSMTKAutoloadAttributeBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKAutoloadAttributeBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKAutoloadAttributeBehavior() override;

protected:
  pqSMTKAutoloadAttributeBehavior(QObject* parent = nullptr);

protected slots:
  void loadAttribute(pqSMTKWrapper*, pqServer* server);

private:
  Q_DISABLE_COPY(pqSMTKAutoloadAttributeBehavior);
};

#endif
