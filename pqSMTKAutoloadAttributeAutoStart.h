//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKAutoloadAttributeAutoStart_h
#define pqSMTKAutoloadAttributeAutoStart_h

#include "Exports.h"

#include <QObject>

/** \brief An entry point into our ParaView plugin
  */
class SMTKAUTOLOADATTRIBUTE_EXPORT pqSMTKAutoloadAttributeAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqSMTKAutoloadAttributeAutoStart(QObject* parent = nullptr);
  ~pqSMTKAutoloadAttributeAutoStart() override;

  /// Method to call when the plugin is loaded
  void startup();

  /// Method to call when the plugin is unloaded
  void shutdown();

private:
  Q_DISABLE_COPY(pqSMTKAutoloadAttributeAutoStart);
};

#endif
