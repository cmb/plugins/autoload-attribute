//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKAutoloadAttributeAutoStart.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKRegisterImportersBehavior.h"

#include "pqSMTKAutoloadAttributeBehavior.h"

#include "pqApplicationCore.h"

pqSMTKAutoloadAttributeAutoStart::pqSMTKAutoloadAttributeAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKAutoloadAttributeAutoStart::~pqSMTKAutoloadAttributeAutoStart()
{
}

void pqSMTKAutoloadAttributeAutoStart::startup()
{
  // We require that the singletons pqSMTKBehavior and
  // pqSMTKRegisterImportersBehavior exist prior to the instantiation of our
  // behavior, so we access/create them now. If smtk's smtkPQComponentsPlugin is
  // loaded first, the two behaviors will already be instantiated and the
  // following calls will have no effect. Otherwise, the accessor methods will
  // implicitlly create the behaviors' singleton instances.
  auto smtkBehavior = pqSMTKBehavior::instance();
  auto registerImportersBehavior = pqSMTKRegisterImportersBehavior::instance();

  // Access/create the singleton instance of our behavior.
  auto autoloadAttributeBehavior = pqSMTKAutoloadAttributeBehavior::instance(this);

  // Register our behavior with the application core.
  //
  // NOTE: depending on plugin load ordering, the above calls to the singleton
  //       instances of pqSMTKBehavior and pqSMTKRegisterImportersBehavior may
  //       create their instances. That does not mean that this plugin assumes
  //       ownership of the behaviors, however. The responsibility for
  //       registering and unregistering the above-mentioned behaviors still
  //       belongs to smtk's smtkPQComponentsPlugin.
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->registerManager("smtk autoload attribute", autoloadAttributeBehavior);
  }
}

void pqSMTKAutoloadAttributeAutoStart::shutdown()
{
  // Unregister our behavior from the application core.
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->unRegisterManager("smtk autoload attribute");
  }
}
