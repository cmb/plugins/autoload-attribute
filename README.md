AutoloadSMTKAttribute
=====================
This project demonstrates how to construct a standalone plugin for CMB
that automatically loads an attribute resource to every server.

Building
========
This plugin requires ParaView, Qt5, SMTK and the smtkPQComponentsExt
plugin to build (as well as their dependencies). Due to CMB's use of
singleton behavior classes, you must link against the same SMTK
libraries as those used in the smtkPQComponentsExt plugin.

License
=======

CMB is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details.

[License.txt]: LICENSE.txt
